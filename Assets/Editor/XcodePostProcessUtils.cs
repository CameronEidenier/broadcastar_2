﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

public static class XcodePostProcessUtils {

	public static string[] copyIgnorePatterns;

	public static void CopyAndReplaceDirectory (string srcPath, string dstPath)
	{
		if (Directory.Exists (dstPath))
			Directory.Delete (dstPath, true);
		if (File.Exists (dstPath))
			File.Delete (dstPath);
		
		Directory.CreateDirectory (dstPath);
		
		foreach (var file in Directory.GetFiles(srcPath)) {
			
			if(copyIgnorePatterns != null && copyIgnorePatterns.Any( p => Regex.IsMatch(file, p) ) == true)
				continue;
			
			File.Copy (file, Path.Combine (dstPath, Path.GetFileName (file)));
		}
		
		foreach (var dir in Directory.GetDirectories(srcPath)) {
			CopyAndReplaceDirectory (dir, Path.Combine (dstPath, Path.GetFileName (dir)));
		}
	}
	
	public static void CopyAndReplaceFile (string srcPath, string dstPath)
	{
		if (Directory.Exists (dstPath))
			Directory.Delete (dstPath, true);
		if (File.Exists (dstPath))
			File.Delete (dstPath);
		
		//Directory.CreateDirectory (dstPath);
		
		File.Copy (srcPath, dstPath);
		
		//		foreach (var file in Directory.GetFiles(srcPath)) {
		//			File.Copy (file, Path.Combine (dstPath, Path.GetFileName (file)));
		//		}
		//
		//		foreach (var dir in Directory.GetDirectories(srcPath)) {
		//			CopyAndReplaceDirectory (dir, Path.Combine (dstPath, Path.GetFileName (dir)));
		//		}
	}
}
